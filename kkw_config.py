"""
config command entry point

common functions and small sets of commands are here
larger sets are implemented in their own file
"""

import re

import click
import toml

import kkw_serial
import kkw_config_set_get
import kkw_config_push
import kkw_config_phone


TOML_DEFAULT = """
what = "configuration for kikiwi.py"

[project]
board = 15100004
station = 18110070

[sd]
is_used = false
name = 'prj_name'

[gsm]
phones = [
  [ '0123456789', 'Albert EINSTEIN' ],
  [ '2357111317', 'Charlie HALL' ],
]

pin = '0000'

start = '01/10/2022@12:43:56'
period = '00:05:00'
sms_nb = 100
alti_sms = 1

[radio]
value = 869.450
power = 14
list = [ 869.450, 869.525, 869.600 ]
"""


def config_file_update(ctx, toml_cfg):
    """update the config file with the given settings"""
    with open(ctx.obj['config'], 'w') as f:
        toml.dump(toml_cfg, f)


@click.group()
@click.pass_context
def config(ctx):
    """configuration handling"""
    # click.echo(f'in config: ctx = {ctx.obj}')

    # load config file
    try:
        toml_cfg = toml.load(ctx.obj['config'])

    except FileNotFoundError:
        # create the file with default values
        toml_cfg = toml.loads(TOML_DEFAULT)
        config_file_update(ctx, toml_cfg)

    # click.echo(f'in config: toml = {toml_cfg}')

    ctx.obj['toml_cfg'] = toml_cfg


@config.command()
@click.pass_context
def check(ctx):
    """check configuration toward connected kikiwi"""
    port = ctx.obj['port']
    kkw = kkw_serial.KKWSerial(port)

    # display version and id
    click.echo(f'version = [{kkw.version}] / id = {kkw.id}')

    toml_cfg = ctx.obj['toml_cfg']
    toml_cfg_id = None
    check_ok = True

    if kkw.is_station:
        toml_cfg_id = toml_cfg['project']['station']
        if int(kkw.id) != toml_cfg_id:
            check_ok = False

    if kkw.is_board:
        toml_cfg_id = toml_cfg['project']['board']
        if int(kkw.id) != toml_cfg_id:
            check_ok = False

    if check_ok:
        click.echo('check [OK]')

    else:
        click.echo(f'configuration and board ids mismatch: {toml_cfg_id}')
        click.echo('consider updating the configuration file')
        click.echo('./kikiwi.py config set [station|board] <new id>')
        click.echo('check [ko]')


@config.command()
@click.pass_context
def test(ctx):
    """test the current config by sending SMS"""
    port = ctx.obj['port']
    kkw = kkw_serial.KKWSerial(port)

    # start the test
    click.secho('make sure a config was pushed to the board', fg='white', bg='bright_red')
    click.secho('stop the test by pressing Ctrl-C', fg='white', bg='bright_red')
    click.echo('wait a few seconds before receiving the test feedback')

    kkw.write(b'GSMT')

    # echo the test output
    while True:
        buf = kkw.readline(raw=True)
        click.echo(buf, nl='')


# setter and getter command set
config.add_command(kkw_config_set_get.set)
config.add_command(kkw_config_set_get.get)

# push command
config.add_command(kkw_config_push.push)

# phone command set
config.add_command(kkw_config_phone.phone)
