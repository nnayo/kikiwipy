python CLI for kikiwi emitter 

# installation

this code is pure python3.
it requires only 3 externals packages:
 - pyserial: to communicate with the device
 - click: to handle the CLI parameters
 - toml: to parse and generate the configuration file

installing python3 and pip is done by default under Linux, on other OSes, please help yourself...

if you don't want to install manually the 3 above packages for the whole system, use a virtual environment.
personnally, I use `pipenv`:
 1. install pipenv: `pip install -U pipenv`
 2. change to the project directory (but since you're reading this file, hopefully, it's done)
 3. run `pipenv shell` to switch to the usage environment
 4. run `pipenv install pyserial click toml`


# typical workflow

the typical workflow is:
 - set a configuration
 - push the configuration to the board
 - test the configuration

once everything is fine, keep a copy of the configuration file, so that it is possible to push to the board on D-day.

if the configuration is fine (the board green led shall be blinking), it's no use pushing it to the board again and again.


## configuration settings

if no config file is present, the command `./kikiwi.py config check` will make sure one is created.
the default config file name is `kikiwi.toml`

to set a configuration, whether use the provided setter via `./kikiwi.py config set <item> <value>` or edit the confi file by hand.


## configuration update

to update the board configuration, use `./kikiwi.py config push`


## configuration test

1. direct connection test:
 - connect the board USB link
 - run `./kikiwi.py config test`

2. station reception test:
 - switch on the board
 - connect the station USb link
 - run `./kikiwi.py rx`


# misc

## position URL format

https://www.openstreetmap.org/?mlat=43,6399500&mlon=001,4262600#map=19/43,6399500/001,4262600#name=simulator


## reverse-engineered protocol

see protocol.md

# what's working

 - configuring the board and the station
 - testing the board configuration
 - receiving frames from the board via the station

# what needs more tests

 - SMS generation (pin code is correctly set, no SMS sent)
 - SD card (when present the red led is blinking but nothing is written)
