"""
config phone command set
"""

import re

import click
import toml

import kkw_serial
import kkw_config


PHONES_MAX_NB = 5


@click.group()
@click.pass_context
def phone(ctx):
    """add/remove phone numbers"""
    ...


@phone.command(name='ls')
@click.pass_context
def phone_ls(ctx):
    """list all new phone numbers"""
    toml_cfg = ctx.obj['toml_cfg']

    for p in toml_cfg['gsm']['phones']:
        click.echo(f'\t{p[0]}: {p[1]}')


@phone.command(name='add')
@click.pass_context
@click.argument('number', metavar='<phone number>', type=str)
@click.argument('name', metavar='<name>', type=str)
def phone_add(ctx, number, name):
    """
    add a new phone number with a name\n
    if the number already exists, the name is overwritten
    """
    toml_cfg = ctx.obj['toml_cfg']
    phones = toml_cfg['gsm']['phones']

    # scan through the list to try to update
    for p in phones:
        if number == p[0]:
            p[1] = name
            kkw_config.config_file_update(ctx, toml_cfg)

            return

    # the number was not in the list
    # but they're already too many numbers
    if len(phones) >= PHONES_MAX_NB:
        click.echo('phone list is full, consider deleting before adding')

        return

    # finally, add the given number
    toml_cfg['gsm']['phones'].append([number, name])
    kkw_config.config_file_update(ctx, toml_cfg)


@phone.command(name='rem')
@click.pass_context
@click.argument('number', metavar='<phone number>', type=str)
def phone_rem(ctx, number):
    """remove a phone number"""
    toml_cfg = ctx.obj['toml_cfg']
    phones = toml_cfg['gsm']['phones']

    # scan through the list to try to remove
    for p in phones:
        if number == p[0]:
            phones.remove(p)
            kkw_config.config_file_update(ctx, toml_cfg)


@phone.command(name='pin')
@click.pass_context
@click.argument('pin', metavar='<pin number>', type=str)
def phone_rem(ctx, pin):
    """set the pin number"""
    toml_cfg = ctx.obj['toml_cfg']
    toml_cfg['gsm']['pin'] = pin

    kkw_config.config_file_update(ctx, toml_cfg)
