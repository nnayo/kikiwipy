#!/usr/bin/env python3

"""
CLI to kikiwi board and station
"""

import serial
import click

import kkw_config
import kkw_rx


# define the common options
@click.group()
@click.pass_context
@click.option('--port', metavar='<port>', type=str, default='/dev/ttyUSB0', show_default=True, help='serial to connect to')
@click.option('--config', metavar='<config file>', type=str, default='./kikiwi.toml', show_default=True, help='file to save or restore the config')
def kikiwi(ctx, port, config):
    """script to interact with kikiwi emitter/station"""
    # click.echo(f'in kikiwi: port = {port}, config = {config}')

    ctx.ensure_object(dict)
    ctx.obj['port'] = port
    ctx.obj['config'] = config


kikiwi.add_command(kkw_config.config)
kikiwi.add_command(kkw_rx.rx)
kikiwi.add_command(kkw_rx.acq)


if __name__ == '__main__':
    kikiwi()
