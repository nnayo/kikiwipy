# station connection

## connection synchro
W -> 24 23 25						= $#%
R <- 23 23 25 0d 0a					= ##%..

## version retrieval
W -> 24 56 3f 25 0d 0a					= $V?%..
R <- 23 4b 69 6b 69 77 69 20 4b 45 59 53 54 41 54 49	= #Kikiwi KEYSTATI
     4f 4e 20 76 31 2e 35 20 2d 20 54 65 6e 75 6d 20	= ON v1.5 - Tenum
     2d 20 44 65 63 20 32 30 31 38 25 0d 0a		= - Dec 2018%..

## identifier retrieval
W -> 24 49 3f 25 0d 0a					= $I?%..
R <- 23 4b 31 38 31 31 30 30 37 30 25 0d 0a		= #K18110070%..

## frequency config
W -> 4 46 26 39 34 35 30 d9 5c cd 39 35 32 35 d9 61	= $F&9450.\.9525.a
     9a 39 36 30 30 d9 66 66 25 0d 0a			= .9600.ff%..
W -> 24 46 3d 39 34 35 30 25 0d 0a			= $F=9450%..


## some configuration ??
W -> 24 4d 50 41 4b 25 0d 0a				= $MPAK%..

# station rx mode

W -> 24 58 25                                          	= $X%

# station deconnection (it works without sending that message when closing the link)

W -> 24 51 25						= $Q%


# board connection

board is always returning frames starting with b'\x11\x60'
but under `vim` use the pattern `/^.. Payload` to get the block of interest

## connection synchro
W -> 24 23 25						= $#%
R <- 23 23 25 0d 0a					= ##%..

## version retrieval
W -> 24 56 3f 25 0d 0a					= $V?%..
R <- 23 4b 69 6b 69 77 69 20 42 4f 41 52 44 20 76 31   	= #Kikiwi BOARD v1
     2e 35 20 2d 20 54 65 6e 75 6d 20 2d               	= .5 - Tenum -
     20 44 65 63 65 6d 62 72 65 20 32 30 31 38 25 0d    = Decembre 2018%.
     0a 						= .

## identifier retrieval
W -> 24 49 3f 25 0d 0a					= $I?%..
R <- 23 42 31 35 31 30 30 30 30 34 25 0d 0a            	= #B15100004%..

## some configuration ?? / order is different from station ??

this frame sets the different possible frequencies
W -> 24 46 26 39 34 35 30 d9 5c cd 39 35 32 35 d9 61	= $F&9450.\.9525.a
     9a 39 36 30 30 d9 66 66 25 0d 0a			= .9600.ff%..
this frame sets the frequency to use
W -> 24 46 3d 39 34 35 30 25 0d 0a			= $F=9450%..
this frame sets ??
W -> 24 4d 50 41 4b 25 0d 0a				= $MPAK%..

## SD commands

### do not use SD card
W -> 24 53 44 30 25 0d 0a                              	= $SD0%..

### use SD card
W -> 24 53 44 31 25 0d 0a                              	= $SD1%..

### project name
this frame sets the project name (here 'rev', up to 8 characters, DOS what can you expect...)
W -> 24 53 44 4e 3d 72 65 76 5f 5f 5f 5f 5f 25 0d 0a   	= $SDN=rev_____%..

## external source ???
W -> 24 58 53 30 25 0d 0a                              	= $XS0%..

## SMS commands
this frame sets the SMS date and others
beware the date and hour format!!!
A=1 means alti-sms is ON
W -> 24 47 53 4d 50 44 3d 32 32 2f 31 32 2f 32 30 31   	= $GSMPD=22/12/201
     35 40 31 32 3a 34 33 52 3d 30 30 33 30 30 4e 3d   	= 5@12:43R=00300N=
     30 30 32 41 3d 30 25 0d 0a                        	= 002A=0%..
or
W -> 24 47 53 4d 50 44 3d 31 30 2f 32 2f 32 30 32 32   	= $GSMPD=10/2/2022
     40 36 3a 35 36 3a 52 3d 36 35 34 36 37 4e 3d 30   	= @6:56:R=65467N=0
     31 31 41 3d 30 25 0d 0a                           	= 11A=0%..

this frame the phone numbers
numbers are up to 10 digits, left padded with 'x'
names are up to 20 characters, left padded with ' '
W -> 24 47 53 4d 53 30 32 25 0d 0a                     	= $GSMS02%..
W -> 24 47 53 4d 46 30 31 32 33 34 35 36 37 38 39 78   	= $GSMF0123456789x
     78 41 6c 62 65 72 74 20 45 49 4e 53 54 45 49 4e   	= xAlbert EINSTEIN
     20 20 20 20 20 20 20 32 33 35 37 31 31 31 33 31	=        235711131
     37 78 78 43 68 61 72 6c 69 65 20 48 41 4c 4c 20   	= 7xxCharlie HALL 
     20 20 20 20 20 20 20 20 20 25 0d 0a		=          %..

or

W -> 24 47 53 4d 53 30 33 25 0d 0a                     	= $GSMS03%..
W -> 24 47 53 4d 46 30 31 32 33 34 35 36 37 38 39 78   	= $GSMF0123456789x
     78 41 6c 62 65 72 74 20 45 49 4e 53 54 45 49 4e   	= xAlbert EINSTEIN
     20 20 20 20 20 20 20 32 33 35 37 31 31 31 33 31   	=        235711131
     37 78 78 43 68 61 72 6c 69 65 20 48 41 4c 4c 20   	= 7xxCharlie HALL 
     20 20 20 20 20 20 20 20 20 37 37 36 36 35 35 78   	=          776655x
     78 78 78 78 78 79 6f 67 20 20 20 20 20 20 20 20   	= xxxxxyog        
     20 20 20 20 20 20 20 20 20 20 20 25 0d 0a                    %..

### GSM pin
this pin code to 7111
W -> 24 47 53 4d 50 49 4e 37 31 31 31 25 0d 0a         	= $GSMPIN7111%..

### GSM test
start a GSM test
W -> 24 47 53 4d 54 25 0d 0a                           	= $GSMT%..

test echo
R <- 2a 2a 2a 20 47 53 4d 20 52 65 61 64 79 0d 0a 41   	= *** GSM Ready..A
     54 2b 43 50 49 4e 3d 22                           	= T+CPIN="
     37 31                                             	= 71
     31 31 22 0d 0a                                    	= 11"..
     23 0d 0a 23 45 52 52 4f 52 0d 0a                  	= #..#ERROR..
     41 54 2b 43 4d 47 46 3d 31 0d 0a 23 0d 0a 23 4f   	= AT+CMGF=1..#..#O
     4b 0d 0a                                          	= K..
     41 54 2b 43 53 4d 50 3d 31 37 2c 31 36 37 2c 30   	= AT+CSMP=17,167,0
     2c 30 0d 0a                                       	= ,0..
     23 0d 0a 23 2b 43 4d 53 20 45 52 52 4f 52 3a 20   	= #..#+CMS ERROR: 
     33 31 30 0d 0a                                    	= 310..
     41 54 2b 43 53 43 41 3f 0d 0a                     	= AT+CSCA?..
     23 0d 0a 23 2b 43 4d 53 20 45 52 52 4f 52 3a 20   	= #..#+CMS ERROR: 
     33 31 30 0d 0a                                    	= 310..
     41 54 2b 43 4d 47 53 3d 22                        	= AT+CMGS="
     30 31 32                                          	= 012
     33 34 35                                          	= 345
     36 37 38                                          	= 678
     39 22 0d 0a                                       	= 9"..
     23 0d 0a 23 2b 43 4d 53 20 45 52 52 4f 52 3a 20   	= #..#+CMS ERROR: 
     33 31 30 0d 0a                                    	= 310..
     72                                                	= r
     65 76 32                                          	= ev2
     5f 5f 5f                                          	= ___
     5f 2d 4b 49                                       	= _-KI
     4b 49 57                                          	= KIW
     49 20 50                                          	= I P
     61 73 5f                                          	= as_
     64 65 5f 6c                                       	= de_l
     6f 63 61                                          	= oca
     6c 69 73                                          	= lis
     61 74 69                                          	= ati
     6f 6e 5f                                          	= on_
     47 50 53                                          	= GPS
     1a                                                	= .
     41 54 2b 43 4d 47 53 3d 22 2b                     	= AT+CMGS="+
     32 33                                             	= 23
     35 37 31                                          	= 571
     31 31 33                                          	= 113
     31 37 22 0d 0a                                    	= 17"..
     23 0d 0a 23 2b 43 4d 53 20 45 52 52 4f 52 3a 20   	= #..#+CMS ERROR: 
     33 31 30 0d 0a                                    	= 310..
     72 65 76 32                                       	= rev2
     5f 5f 5f                                          	= ___
     5f 2d 4b                                          	= _-K
     49 4b 49                                          	= IKI
     57 49 20                                          	= WI
     50 61 73 5f                                       	= Pas_
     64 65 5f                                          	= de_
     6c 6f 63                                          	= loc
     61 6c 69                                          	= ali
     73 61 74                                          	= sat
     69 6f 6e                                          	= ion
     5f 47 50 53                                       	= _GPS
     1a                                                	= .
     41 54 2b 43 4d 47 53 3d 22 2b                     	= AT+CMGS="+
     31                                                	= 1
     32 33 34 35                                       	= 2345
     36 37 38                                          	= 678
     39 30 22 0d 0a                                    	= 90"..
     23 0d 0a 23 2b 43 4d 53 20 45 52 52 4f 52 3a 20   	= #..#+CMS ERROR: 
     33 31 30 0d 0a                                    	= 310..
     72 65 76                                          	= rev
     32 5f 5f                                          	= 2__
     5f 5f 2d                                          	= __-
     4b 49 4b                                          	= KIK
     49 57 49 20                                       	= IWI 
     50 61 73                                          	= Pas
     5f 64 65                                          	= _de
     5f 6c 6f                                          	= _lo
     63 61 6c                                          	= cal
     69 73 61 74                                       	= isat
     69 6f 6e                                          	= ion
     5f 47 50                                          	= _GP
     53                                                	= S
     1a                                                	= .

## single data acquisition
W -> 24 44 3f 25                                       	= $D?%
R <- 23 4d 30 30 30 30 35 37 3b 30 30 30 33 3b 30 30   	= #M000057;0003;00
     30 30 3b 30 30 30 30 3b 30 30 30 30 3b 30 30 30   	= 00;0000;0000;000
     30 3b 30 30 30 30 3b 30 30 30 30 3b 30 30 30 30   	= 0;0000;0000;0000
     3b 25 0d 0a                                       	= ;%..

## quit/deconnection command
W -> 24 51 25                                           = $Q%
