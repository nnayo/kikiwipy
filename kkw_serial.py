"""
kikiwi serial interface seems a bit slow
to prevent any hang-up during dialog, a delay is systematically added before any write

on any error, an exception is raised
"""

import serial
import time


class KKWSerialException(Exception):
    pass


class KKWSerial:
    def __init__(self, port):
        self._ser = serial.Serial(port, 115200)

        # check synchro
        self._ser.write(b'$#%')  # raw write since trailer is partial for synchro

        # wait and check synchro response
        resp = self.readline()
        if resp != '#':
            raise KKWSerialException(f'bad synchro, resp = {resp}')

        # retrieve version
        self.write(b'V?')

        self.version = self.readline()
        # version responses look like:
        #  - 'Kikiwi BOARD v1.5 - Tenum - Decembre 2018'
        #  - 'Kikiwi KEYSTATION v1.5 - Tenum - Dec 2018'
        if 'KEYSTATION' in self.version:
            self.is_station, self.is_board = True, False
        elif 'BOARD' in self.version:
            self.is_station, self.is_board = False, True
        else:
            raise KKWSerialException(f'unknown item: {self.version}')
 
        # retrieve id
        self.write(b'I?')

        self.id = self.readline()
        # identifier reponses look like:
        #  - 'B15100004'  <-- for board
        #  - 'K18110070'  <-- for keystation
        if 'K' in self.id and self.is_board:
            raise KKWSerialException(f'version and id mismatch, version = {self.version}, id = {self.id}')

        if 'B' in self.id and self.is_station:
            raise KKWSerialException(f'version and id mismatch, version = {self.version}, id = {self.id}')

        # suppress leading letter, the type is already extracted in self.is_board or self.is_station
        self.id = self.id[1:]

        # force unknown default config
        self._default_config()

    def _default_config(self):
        """set a default config, so far it is unknown what's doing"""
        self.write(b'MPAK')

    def write(self, data):
        """send data but add a delay to prevent overflowing the kikiwi"""
        time.sleep(0.1)

        # add header and trailer to the data to send
        self._ser.write(b'$' + data + b'%\r\n')

    def readline(self, raw=False):
        """retrieve a full frame and check it"""
        frm = self._ser.readline().decode()

        # raw mode is used for board test
        if raw:
            return frm

        return self._frame_check_and_clean(frm, raw)

    def _frame_check_and_clean(self, frm, raw):
        """check and remove header and trailer"""
        if frm[0] != '#':
            raise KKWSerialException(f'bad header in {frm[:-2]}: {frm[0]} != #')

        if frm[-3:] != '%\r\n':
            raise KKWSerialException(f'bad trailer: in {frm[:-2]} {frm[-3:]} != %\\r\\n')

        return frm[1:-3]
