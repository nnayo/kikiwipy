"""
configuration setters and getters
"""

import re

import click
import toml

import kkw_serial


@click.group()
@click.pass_context
def get(ctx):
    """get item from configuration file"""
    cfg = ctx.obj['config']


@get.command(name='all')
@click.pass_context
def get_all(ctx):
    """display the configuration file"""
    cfg = ctx.obj['config']

    click.echo('TODO')


@get.command(name='board')
@click.pass_context
def get_board(ctx):
    """get board"""
    toml_cfg = ctx.obj['toml_cfg']
    board_id = toml_cfg['project']['board']

    click.echo(f'{board_id}')
    return board_id


@get.command(name='station')
@click.pass_context
def get_station(ctx):
    """get station"""
    toml_cfg = ctx.obj['toml_cfg']
    station_id = toml_cfg['project']['station']

    click.echo(f'{station_id}')
    return station_id


@get.command(name='start')
@click.pass_context
def get_start(ctx):
    """get start date"""
    toml_cfg = ctx.obj['toml_cfg']
    start = toml_cfg['gsm']['start']

    click.echo(f'{start}')
    return start


@get.command(name='period')
@click.pass_context
def get_period(ctx):
    """get SMS period"""
    toml_cfg = ctx.obj['toml_cfg']
    period = toml_cfg['gsm']['period']

    click.echo(f'{period}')
    return period


@get.command(name='sms_nb')
@click.pass_context
def get_sms_nb(ctx):
    """get number of SMS to send"""
    toml_cfg = ctx.obj['toml_cfg']
    sms_nb = toml_cfg['gsm']['sms_nb']

    click.echo(f'{sms_nb}')
    return sms_nb


@get.command(name='alti_sms')
@click.pass_context
def get_sms_nb(ctx):
    """get number of SMS to send"""
    toml_cfg = ctx.obj['toml_cfg']
    sms_nb = toml_cfg['gsm']['sms_nb']

    click.echo(f'{sms_nb}')
    return sms_nb


@get.command(name='freq')
@click.pass_context
def get_freq(ctx):
    """get used frequency"""
    toml_cfg = ctx.obj['toml_cfg']
    freq = toml_cfg['radio']['value']

    click.echo(f'{freq:3.3f} MHz')
    return freq


@click.group()
@click.pass_context
def set(ctx):
    """set configuration"""
    ...


@set.command(name='board')
@click.pass_context
@click.argument('board_id', metavar='<board id>', type=int)
def set_board(ctx, board_id):
    """set board id"""
    toml_cfg = ctx.obj['toml_cfg']
    toml_cfg['project']['board'] = board_id

    config_file_update(ctx, toml_cfg)


@set.command(name='station')
@click.pass_context
@click.argument('station_id', metavar='<station id>', type=int)
def set_station(ctx, station_id):
    """set station id"""
    toml_cfg = ctx.obj['toml_cfg']
    toml_cfg['project']['station'] = station_id

    config_file_update(ctx, toml_cfg)


@set.command(name='start')
@click.pass_context
@click.argument('date', metavar='<date>', type=str)
def set_start(ctx, date):
    """set start date in format DD/MM/YYYY@HH:MM:SS"""
    # check format
    if not re.match(r'\d\d/\d\d/\d\d\d\d@\d\d:\d\d:\d\d', date):
        click.echo(f'invalid date format {date}, shall match DD/MM/YYYY@HH:MM:SS')
        return

    toml_cfg = ctx.obj['toml_cfg']
    toml_cfg['gsm']['start'] = date

    config_file_update(ctx, toml_cfg)


@set.command(name='period')
@click.pass_context
@click.argument('period', metavar='<period>', type=str)
def set_period(ctx, period):
    """set period between SMS send in format HH:MM:SS"""
    # check format
    if not re.match(r'\d\d:\d\d:\d\d', period):
        click.echo(f'invalid period format {period}, shall match HH:MM:SS')
        return

    toml_cfg = ctx.obj['toml_cfg']
    toml_cfg['gsm']['period'] = period

    config_file_update(ctx, toml_cfg)


@set.command(name='sms_nb')
@click.pass_context
@click.argument('nb', metavar='<number>', type=int)
def set_sms_nb(ctx, nb):
    """set total number of SMS to send"""
    toml_cfg = ctx.obj['toml_cfg']
    toml_cfg['gsm']['sms_nb'] = nb

    config_file_update(ctx, toml_cfg)


@set.command(name='freq')
@click.pass_context
@click.argument('freq', metavar='<freq in MHz>', type=float)
def set_freq(ctx, freq):
    """set transmitter frequency to use"""
    toml_cfg = ctx.obj['toml_cfg']

    # check is available
    if freq not in toml_cfg['radio']['list']:
        (f0, f1, f2) = toml_cfg['radio']['list']
        click.echo(f'frequency shall be one of these: {f0:3.3f}, {f1:3.3f}, {f2:3.3f}')
        return

    toml_cfg['radio']['value'] = freq
    config_file_update(ctx, toml_cfg)
