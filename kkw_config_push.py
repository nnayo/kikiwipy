"""
configuration push command
"""

import re

import click
import toml

import kkw_serial


def push_freq(ctx, kkw):
    """push the frequency list and the set frequency"""
    toml_cfg = ctx.obj['toml_cfg']
    radio = toml_cfg['radio']

    def freq_format(freq):
        freq -= 860
        freq *= 1000

        return f'{int(freq):04d}'.encode()

    freqs = b'F&' + \
            freq_format(radio['list'][0]) + b'\xd9\x5c\xcd' + \
            freq_format(radio['list'][1]) + b'\xd9a\x9a' + \
            freq_format(radio['list'][2]) + b'\xd9ff'
    kkw.write(freqs)

    freq = b'F=' + freq_format(radio['value'])
    kkw.write(freq)


def push_gsm(ctx, kkw):
    """push gsm settings and phone numbers"""
    toml_cfg = ctx.obj['toml_cfg']

    def period_format(period):
        """transform period from HH:MM:SS format to seconds"""
        hh, mm, ss = period.split(':')

        duration = int(ss)
        duration += 60 * int(mm)
        duration += 60 * 60 * int(hh)

        return f'{duration:05d}'.encode()

    def sms_nb_format(sms_nb):
        """transform SMS number"""
        return f'{sms_nb:03d}'.encode()

    # general settings
    gsm = toml_cfg['gsm']

    settings = b'GSMPD=' + gsm['start'].encode() + \
               b'R=' + period_format(gsm['period']) + \
               b'N=' + sms_nb_format(gsm['sms_nb']) + \
               b'A='
    settings += b'1' if gsm['alti'] else b'0'

    kkw.write(settings)

    # phones numbers
    phones = gsm['phones']
    phone_frm = b'GSMS' + f'{len(phones):02d}'.encode()

    kkw.write(phone_frm)

    phone_list = b'GSMF'
    for p in phones:
        phone_list += f'{p[0][:12]}'.ljust(12, 'x').encode() + \
                      f'{p[1][:20]}'.ljust(21).encode()

    kkw.write(phone_list)

    # pin code
    pin = gsm['pin']
    pin_frm = b'GSMPIN' + f'{pin}'.encode()

    kkw.write(pin_frm)


def push_sd(ctx, kkw):
    """push SD settings"""
    toml_cfg = ctx.obj['toml_cfg']
    sd = toml_cfg['sd']

    sd_save = b'SD'
    sd_save += b'1' if sd['is_used'] else b'0'

    kkw.write(sd_save)

    dir_name = sd['name'].ljust(8, '_').encode()
    sd_dir = b'SDN=' + dir_name

    kkw.write(sd_dir)


def push_station(ctx, kkw):
    """push configuration to station"""
    push_freq(ctx, kkw)


def push_board(ctx, kkw):
    """push configuration to board"""
    push_freq(ctx, kkw)
    push_gsm(ctx, kkw)
    push_sd(ctx, kkw)


@click.command()
@click.pass_context
def push(ctx):
    """push configuration from file to board"""
    port = ctx.obj['port']
    kkw = kkw_serial.KKWSerial(port)

    if kkw.is_station:
        click.echo('pushing to station...', nl=False)
        push_station(ctx, kkw)

    if kkw.is_board:
        click.echo('pushing to board...', nl=False)
        push_board(ctx, kkw)

    click.echo(' [OK]')
