"""
data frame reception handling
"""

from enum import Enum
import time

import click

import kkw_serial


# expected frame length in command mode
FRAME_CMD_LEN = 48

# expected frame length in reception mode
FRAME_RX_LEN = 52

# offset in frame of the type
FRAME_TYPE_OFFSET = 0


class FrameType(Enum):
    MESSAGE = 'M'
    GNSS = 'G'
    SMS = 'S'
    EXTERNAL = 'X'


def measure_extract(buf, with_url=False):
    """measure format is 'M<time tag>;c0;c1;c2;c3;c4;c5;c7;'"""
    tag, *mes, rssi = buf.split(';')

    click.echo(f'{tag[0:2]}:{tag[2:4]}:{tag[4:6]} ', nl=False)

    for m in mes:
        v = int(m)
        if v < 10 or v > 3000 - 10:
            color = 'bright_red'
        elif v < 50 or v > 3000 - 50:
            color = 'bright_yellow'
        else:
            color = 'bright_green'

        click.secho(f'{v:4d} mV, ', nl=False, fg=color)

    # when the measure is received via the serial link, RSSI is missing
    try:
        click.echo(f'rssi = -{int(rssi):3d} dB')
    except ValueError:
        click.echo('')


def gnss_extract(buf, with_url):
    # separator between nb sats and rssi can be '_' or '!'
    buf = buf.replace('_', ';')
    buf = buf.replace('!', ';')
    try:
        date, hour, lat, long, alt, sats, rssi = buf.split(';')
    except ValueError:
        click.secho('no position', fg='red')
        return

    date = '20' + date[4:6] + '/' + date[2:4] + '/' + date[0:2] + ' '
    click.echo(date, nl=False)

    hour = hour[0:2] + ':' + hour[2:4] + ':' + hour[4:6] + ' '
    click.echo(hour, nl=False)

    n_s = lat[-1]
    lat = float(lat[:-1]) / 100
    click.echo(f'{lat:10f} {n_s} ', nl=False)

    e_o = long[-1]
    long = float(long[:-1]) / 100
    click.echo(f'{long:10f} {e_o} ', nl=False)

    # sometime, the altitude field is like '0-269' or '0000M'
    try:
        click.echo(f'/ alt = {int(alt):5d} m ', nl=False)
    except ValueError:
        click.secho(f'/ alt = ????? m ', fg='red', nl=False)

    try:
        click.echo(f'#sat = {int(sats):2d} ', nl=False)
    except ValueError:
        click.secho(f'#sat = ?? ', fg='red', nl=False)

    click.echo(f'rssi = -{int(rssi):3d} dB ', nl=False)

    # generate an OSM URL
    if with_url:
        click.echo(f'/ [URL] https://www.openstreetmap.org/?mlat={lat:.5f}&mlon={long:.5f}')
    else:
        click.echo('')


def sms_extract(buf, with_url):
    click.echo(' sms: decoding to be done')


def external_extract(buf, with_url):
    click.echo(' external frame: no decoding')


@click.command()
@click.pass_context
@click.option('--with-url', metavar='<URL>', type=bool, is_flag=True, required=False, default=False, help='generated OSM URL for position frames')
def rx(ctx, with_url):
    """wait and parse received frames"""
    port = ctx.obj['port']
    kkw = kkw_serial.KKWSerial(port)

    # enable rx mode
    kkw.write(b'$X%')

    while True:
        frame_rx_n_parse(kkw, with_url=with_url, frm_len=FRAME_RX_LEN)


@click.command()
@click.pass_context
@click.option('--period', metavar='<period>', type=int, required=False, default=1, help='period between measure acquisition')
@click.option('--samples', metavar='<samples>', type=int, required=False, default=5, help='number of measures to acquire')
def acq(ctx, period, samples):
    """request <samples> measures every <period> seconds and parse received frames"""
    click.echo(f'acquiring {samples} sample(s) every {period} second(s)')

    port = ctx.obj['port']
    kkw = kkw_serial.KKWSerial(port)

    for _ in range(samples):
        # request a measure
        kkw.write(b'D?')

        # check and decode the received frame
        frame_rx_n_parse(kkw)

        # wait for a period
        time.sleep(period - 0.1)


def frame_rx_n_parse(kkw, with_url=False, frm_len=FRAME_CMD_LEN):
    """reception and decoding of a frame"""
    frame_type_to_extractor = {
        FrameType.MESSAGE: measure_extract,
        FrameType.GNSS: gnss_extract,
        FrameType.SMS: sms_extract,
        FrameType.EXTERNAL: external_extract,
    }

    # get some data
    buf = kkw.readline()
    click.secho(f'{buf} -> ', nl=False, fg='bright_blue')

    # check if the block is correctly formatted
    if len(buf) != frm_len:
        click.echo(f'invalid length {len(buf)} != frm_len = {frm_len}')
        return

    # extract data upon frame type
    try:
        frame_type = FrameType(buf[FRAME_TYPE_OFFSET])
    except ValueError:
        click.echo(f' unknown frame type: {frame_type}')
        return

    frame_type_to_extractor[frame_type](buf[1:], with_url)
